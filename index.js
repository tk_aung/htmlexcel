const rowCount = 100;
const columnCount = 100;

var allValues = []

let lastSelectedId;

// var supportedFunctions = ['SUM'];
// var supportedOperators = ['-', '+', 'x', '/'];

function initializeValues(rowCount = 0, columnCount = 0) {
    allValues = []
    for (i = 0; i < rowCount; i++) {
        var cellValues = [];
        for (j = 0; j < columnCount; j++) {
            cellValues.push(
                {
                    row: i,
                    column: j,
                    value: 0,
                    formula: '',
                    bold: false,
                    italic: false,
                    underline: false
                }
            );
        }
        allValues.push(cellValues)
    }
}

function toColumnLetters(num) {
    var mod = num % 26,
        pow = num / 26 | 0,
        out = mod ? String.fromCharCode(64 + mod) : (--pow, 'Z');
    return pow ? toColumnLetters(pow) + out : out;
}

function fromColumnLetters(str) {
    var out = 0, len = str.length, pos = len;
    while (--pos > -1) {
        out += (str.charCodeAt(pos) - 64) * Math.pow(26, len - 1 - pos);
    }
    return out;
}

function evaluateStringEquation(fn) {
    return new Function('return ' + fn)();
}

function resolveFormula(formula = '') {
    // NEED comprehensive improvement, probably with Regex to filter formula format
    formula = formula.toUpperCase().replace(/\s+/g, '');
    formula = formula.substring(1);
    let functionKey = ['SUM'].find(x => formula.startsWith(x))
    let equation = []
    let result = 0;

    if (functionKey) {
        let isCommaSeparated = formula.includes(',') // Assuming brackets are there
        if (isCommaSeparated) {
            let cellIds = formula.substring(functionKey.length + 1, formula.length - 1).split(',');
            switch (functionKey) {
                case 'SUM':
                    cellIds.forEach((x, i) => {
                        let rc = x.split(/([0-9]*)/g);
                        let column = fromColumnLetters(rc.filter(i => isNaN(i)).join('')) - 1;
                        let row = parseInt(rc.filter(i => !isNaN(i)).join('')) - 1;
                        equation.push(`${allValues[row][column].value}`);
                        if (i < cellIds.length - 1)
                            equation.push('+')
                    })
                    result = evaluateStringEquation(equation.join(''));
            }
            return result;
        }
        let isColumnSeparated = formula.includes(':') // Assuming brackets are there
        if (isColumnSeparated) {
            let cellIds = formula.substring(functionKey.length + 1, formula.length - 1).split(':');
            if (cellIds.length > 2) return 'undefined';
            switch (functionKey) {
                case 'SUM':
                    let first = cellIds[0].split(/([0-9]*)/g);
                    let first_column = fromColumnLetters(first.filter(i => isNaN(i)).join('')) - 1;
                    let first_row = parseInt(first.filter(i => !isNaN(i)).join('')) - 1;

                    let second = cellIds[1].split(/([0-9]*)/g);
                    let second_column = fromColumnLetters(second.filter(i => isNaN(i)).join('')) - 1;
                    let second_row = parseInt(second.filter(i => !isNaN(i)).join('')) - 1;

                    for (i = 0; i < rowCount; i++) {
                        for (j = 0; j < columnCount; j++) {
                            if ((i >= first_row) && (j >= first_column) && (i <= second_row) && (j <= second_column)) {
                                result = result + parseInt(allValues[i][j].value);
                            }
                        }
                    }
                    break;
                default:
                    result = 0;
            }
            return result;
        }
    }

    let cellIds = formula.split(/([\+\*\/-])/g);

    cellIds.forEach(x => {
        if (x.match(/([\+\*\/-])/g)) {
            equation.push(x);
        } else {
            let rc = x.split(/([0-9]*)/g);
            let column = fromColumnLetters(rc[0]) - 1;
            let row = parseInt(rc[1]) - 1;

            equation.push(`${allValues[row][column].value}`);
        }
    })
    result = evaluateStringEquation(equation.join(''));
    return result;
}

function refreshAllCellText() {
    // TODO: design some kind of association property to only update necessary cells
    // Brute force to counter multiple cell dependencies
    allValues.forEach((x, i) => {
        x.forEach((y, j) => {
            if (y.formula) {
                y.value = resolveFormula(y.formula);
            }
        })
    })
    for (i = 0; i < rowCount; i++) {
        for (j = 0; j < columnCount; j++) {
            let textElement = document.getElementById(`${i}_${j}_text`);
            textElement.innerHTML = allValues[i][j].value;
        }
    }
}

function cellSelected(element) {
    if (lastSelectedId) onCellDeselect(document.getElementById(lastSelectedId))
    lastSelectedId = element.id;

    // use indexes instead of given element to simplify
    rcIndex = element.id.split('_');

    // hide the text
    let textElement = document.getElementById(`${rcIndex[0]}_${rcIndex[1]}_text`);
    textElement.style.display = 'none';

    // highlight the input cell
    let inputDivElement = document.getElementById(`${rcIndex[0]}_${rcIndex[1]}_input_div`);
    inputDivElement.style.display = 'flex';

    let inputElement = document.getElementById(`${rcIndex[0]}_${rcIndex[1]}_input`);
    inputElement.focus();
}

function onCellDeselect(element) {
    // use indexes instead of given element to simplify
    rcIndex = element.id.split('_');

    // hide the text
    let textElement = document.getElementById(`${rcIndex[0]}_${rcIndex[1]}_text`);
    textElement.style.display = 'flex';

    // highlight the input cell
    let inputElement = document.getElementById(`${rcIndex[0]}_${rcIndex[1]}_input_div`);
    inputElement.style.display = 'none';
}


function cellValueChange(event) {
    if (event.target && event.target.id.includes('input')) {
        let rcIndexes = event.target.id.split('_');
        if (event.target.value[0] === '=') {
            // if input is formula store the formula and refresh the value
            allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].formula = event.target.value;
            allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].value = undefined;
        } else {
            allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].value = event.target.value;
            allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].formula = undefined;
        }

        let t = document.getElementById(event.target.id);
        onCellDeselect(document.getElementById(event.target.id))

        // TODO: Refresh calculation and UI Elements 
        refreshAllCellText();
    }
}

function boldClick(e) {
    // use indexes instead of given element to simplify
    rcIndexes = e.id.split('_');
    allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].bold = !allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].bold;

    document.getElementById(`${rcIndex[0]}_${rcIndex[1]}_input`).style.fontWeight = allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].bold ? 'bold' : 'normal';
    document.getElementById(`${rcIndex[0]}_${rcIndex[1]}_text`).style.fontWeight = allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].bold ? 'bold' : 'normal';
}

function italicClick(e) {
    // use indexes instead of given element to simplify
    rcIndexes = e.id.split('_');
    allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].italic = !allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].italic;

    document.getElementById(`${rcIndex[0]}_${rcIndex[1]}_input`).style.fontStyle = allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].italic ? 'italic' : 'normal';
    document.getElementById(`${rcIndex[0]}_${rcIndex[1]}_text`).style.fontStyle = allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].italic ? 'italic' : 'normal';
}

function underlineClick(e) {
    // use indexes instead of given element to simplify
    rcIndexes = e.id.split('_');
    allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].underline = !allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].underline;

    document.getElementById(`${rcIndex[0]}_${rcIndex[1]}_input`).style.textDecoration = allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].underline ? 'underline' : 'normal';
    document.getElementById(`${rcIndex[0]}_${rcIndex[1]}_text`).style.textDecoration = allValues[parseInt(rcIndexes[0])][parseInt(rcIndexes[1])].underline ? 'underline' : 'normal';
}

function createTable() {

    /*
        <body>
            <table>
                <tHead>
                    <tr>
                        <td>A</td>
                        <td>..</td>
                    </tr>
                </tHead>
                <tBody>
                    <tr>
                        <td>
                            <div>
                                <div>1</div>
                                <div>
                                    <input>
                                    <div>
                                        <btn>b</btn>
                                        <btn>i</btn>
                                        <btn>u</btn>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>...</td>
                    </tr>
                </tBody>
            </table>
        </body>
     */
    var body = document.body;

    const table = document.createElement('table');
    table.setAttribute('id', 'mainTable');

    // Add Column Header (A, B, C, ...)
    const header = table.createTHead();
    var headerRow = header.insertRow();
    headerRow.insertCell().innerHTML = ''
    for (j = 0; j < allValues[0].length; j++) {
        let columnHeader = headerRow.insertCell();
        columnHeader.className = 'headers';
        columnHeader.innerHTML = toColumnLetters(j + 1);
    }

    const tbody = table.createTBody();
    for (i = 0; i < allValues.length; i++) {
        let row = tbody.insertRow();

        // Insert Row Header
        let rowHeader = row.insertCell();
        rowHeader.className = 'headers';
        rowHeader.innerHTML = i + 1;

        for (j = 0; j < allValues[i].length; j++) {
            var cell = row.insertCell();
            cell.classList = 'contents';

            let divElement = document.createElement('div');
            divElement.className = 'cellContentDiv';

            let textElement = document.createElement('div')
            textElement.className = 'textDiv'
            textElement.setAttribute('id', `${i}_${j}_text`);
            textElement.setAttribute('onClick', 'cellSelected(this)');
            textElement.innerHTML = allValues[i][j].value;

            let inputDiv = document.createElement('div');
            inputDiv.className = 'inputDiv';
            inputDiv.setAttribute('id', `${i}_${j}_input_div`);
            // inputDiv.style.height = '0px';
            inputDiv.style.display = 'none';
            
            let inputElement = document.createElement('input');
            inputElement.setAttribute('id', `${i}_${j}_input`);
            inputElement.setAttribute('value', allValues[i][j].formula);
            inputElement.style.background = '#9e9e9e5e';

            let formatDiv = document.createElement('div');
            formatDiv.setAttribute('id', `${i}_${j}_input_format_div`);
            formatDiv.style.display = 'flex';
            formatDiv.style.justifyContent = 'space-around';

            let bold = document.createElement('button');
            bold.innerHTML = 'b';
            bold.className = 'boldBtn';
            bold.setAttribute('id', `${i}_${j}_input_bold`);
            bold.style.width = '30px';
            bold.setAttribute('onClick', 'boldClick(this)');

            let italic = document.createElement('button');
            italic.innerHTML = 'i';
            italic.className = 'italicBtn'
            italic.setAttribute('id', `${i}_${j}_input_italic`);
            italic.style.width = '30px';
            italic.setAttribute('onClick', 'italicClick(this)');

            let underline = document.createElement('button');
            underline.innerHTML = 'u';
            underline.className = 'underlineBtn'
            underline.setAttribute('id', `${i}_${j}_input_underline`);
            underline.style.width = '30px';
            underline.setAttribute('onClick', 'underlineClick(this)');

            formatDiv.appendChild(bold);
            formatDiv.appendChild(italic);
            formatDiv.appendChild(underline);

            inputDiv.appendChild(inputElement);
            inputDiv.appendChild(formatDiv);

            divElement.appendChild(textElement);
            divElement.appendChild(inputDiv);

            divElement.setAttribute('id', `${i}_${j}_div`);

            cell.innerHTML = divElement.outerHTML;
        }
    }

    body.appendChild(table);

    for (i = 0; i < allValues.length; i++) {
        for (j = 0; j < allValues[i].length; j++) {
            let a = document.getElementById(`${i}_${j}_input`);
            a.addEventListener('change', e => cellValueChange(e))
        }
    }
}

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

function refresh() {
    removeAllChildNodes(document.body);
    init();
}

function init() {
    initializeValues(rowCount, columnCount);
    let refreshButton = document.createElement('button');
    refreshButton.className = 'refreshBtn';
    refreshButton.setAttribute('onClick', 'refresh()')
    refreshButton.innerHTML = 'Refresh';

    document.body.appendChild(refreshButton);
    createTable();
}

init();



